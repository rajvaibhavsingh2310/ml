# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import tarfile
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle

# Config the matplotlib backend as plotting inline in IPython

num_classes = 2
np.random.seed(133)
last_percent_reported = None
data_root = '.' # Change me to store data elsewhere
global train_data_dog,train_data_cat,validation_data_cat,validation_data_dog,test_data
with open("train/dogs.pickle", 'rb') as f:
    train_data_dog = pickle.load(f)
with open("train/cats.pickle", 'rb') as f:
    train_data_cat = pickle.load(f)
with open("validation/dogs.pickle", 'rb') as f:
    validation_data_dog = pickle.load(f)
with open("train/cats.pickle", 'rb') as f:
    validation_data_cat = pickle.load(f)
with open("test.pickle", 'rb') as f:
    test_data = pickle.load(f)
global train_label_dog,train_label_cat,validation_label_cat,validation_label_dog,test_label
global train_data,train_label,validation_data,validation_label,test_data,test_label
train_data=np.vstack([train_data_dog,train_data_cat])
validation_data=np.vstack([validation_data_dog,validation_data_cat])
train_label=np.append(train_label_dog,train_label_cat)
validation_label=np.append(validation_label_dog,validation_label_cat)
from sklearn.utils import shuffle
train_data, train_label = shuffle(train_data, train_label, random_state=0)
validation_data, validation_label = shuffle(validation_data, validation_label, random_state=0)
pickle_file = 'dogCat.pickle'

try:
  f = open(pickle_file, 'wb')
  save = {
    'train_data': train_data,
    'train_label': train_label,
    'validation_data': validation_data,
    'validation_label': validation_label,
    'test_data': test_data,
    'test_label': test_label,
    }
  pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
  f.close()
except Exception as e:
  print('Unable to save data to', pickle_file, ':', e)
  raise
